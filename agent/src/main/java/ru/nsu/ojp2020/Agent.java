package ru.nsu.ojp2020;

import javassist.CannotCompileException;
import javassist.NotFoundException;
import javassist.bytecode.BadBytecode;

import java.io.IOException;
import java.lang.instrument.Instrumentation;

public class Agent {
    public static void premain(String argument, Instrumentation instrumentation) throws BadBytecode, NotFoundException, CannotCompileException, IOException {
        int count = instrumentation.getAllLoadedClasses().length;

        System.out.println("Classes loaded: " + count);

        instrumentation.addTransformer(new ClassTransformer());
    }
}
