package ru.nsu.ojp2020;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import javassist.NotFoundException;

import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

public class ClassTransformer implements ClassFileTransformer {

    public byte[] transform(ClassLoader loader, String className,
                            Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
                            byte[] classfileBuffer) throws IllegalClassFormatException {
        final String classNameDots = className.replaceAll("/", ".");
        final CtClass ctClass = ClassPool.getDefault().getOrNull(classNameDots);
        if (ctClass == null) return classfileBuffer;
        if (!ctClass.getSimpleName().equals("TransactionProcessor")) {
            return classfileBuffer;
        }

        try {
            ctClass.addField(CtField.make("public static long sumTime;", ctClass), "0L");
            ctClass.addField(CtField.make("public static long minTime;", ctClass), "100000L");
            ctClass.addField(CtField.make("public static long maxTime;", ctClass), "0L");

            final CtMethod transactionMethod = ctClass.getDeclaredMethod("processTransaction");

            transactionMethod.addLocalVariable("time", CtClass.longType);
            transactionMethod.insertBefore("txNum += 99;");
            transactionMethod.insertBefore("time = System.currentTimeMillis();");

            transactionMethod.insertAfter("time = System.currentTimeMillis() - time;");
            transactionMethod.insertAfter("if (time > maxTime) maxTime = time;");
            transactionMethod.insertAfter("if (time < minTime) minTime = time;");
            transactionMethod.insertAfter("sumTime += time;");

            final CtMethod mainMethod = ctClass.getDeclaredMethod("main");

            mainMethod.insertAfter("System.out.println(\"Average time is \" + ru.nsu.ojp2020.TransactionProcessor.sumTime / 10.0);");
            mainMethod.insertAfter("System.out.println(\"Max time is \" + ru.nsu.ojp2020.TransactionProcessor.maxTime);");
            mainMethod.insertAfter("System.out.println(\"Min time is \" + ru.nsu.ojp2020.TransactionProcessor.minTime);");

            return ctClass.toBytecode();
        } catch (CannotCompileException | NotFoundException | IOException e) {
            e.printStackTrace();
            throw new IllegalClassFormatException();
        }
    }
}
