#!/bin/bash

mvn clean install
mkdir "runDir"
cp agent/target/agent-1.0-jar-with-dependencies.jar runDir/agent.jar
cp transaction-processor/target/transaction-processor-1.0-jar-with-dependencies.jar runDir/transaction-processor.jar

cd runDir
java -javaagent:agent.jar -jar transaction-processor.jar
